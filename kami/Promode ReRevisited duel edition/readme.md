Kami's version of Promode ReRevisited mod, reworked to make it suitable for duels (1v1 games).

# CHANGELOG

|  NEW SETTINGS         |      ORIGINAL SETTINGS
|---------------------- | ------------------------
|***weapon***
|name: "AUTO SHOTGUN"   |    name: "AUTO SHOTGUN"
|delay: 20,             |    delay: 28,
|***wobject***
|hitDamage: 1.3,        |    hitDamage: 1,
|***weapon***
|name: "CHAINGUN"       |    name: "CHAINGUN"
|bulletSpeed: 4.2,      |    bulletSpeed: 4,
|ammo: 25,              |    ammo: 50,
|***wobject***
|hitDamage: 7,          |    hitDamage: 4,
|blowAway: 0,           |    blowAway: 0.05,
|***weapon***
|name: "LIGHTNING GUN"  |    name: "LIGHTNING GUN"
|ammo: 120,             |    ammo: 60,
|***wobject***
|timeToExplo: 180,      |    timeToExplo: 140,
|hitDamage: 2.2,        |    hitDamage: 1.8,
|blowAway: 0,           |    blowAway: 0.12,
|***weapon***
|name: "GRN LAUNCHER"   |    name: "GRN LAUNCHER"
|***wobject***
|hitDamage: 20,         |    hitDamage: 4,
|timeToExplo: 180,      |    timeToExplo: 140,
|***weapon***           |
|name: "GRENADE"        |    name: "GRENADE",
|loadingTime: 160,      |    loadingTime: 200,
|***nobject***
|timeToExplo: 17,       |    timeToExplo: 15,
|***weapon***           |
|name: "DOOMSDAY"       |    name: "DOOMSDAY",
|parts: 4,              |    parts: 2,
|bulletSpeed: 2.6,      |    bulletSpeed: 3.5,
|ammo: 7,               |    ammo: 8,
|loadingTime: 440,      |    loadingTime: 400,
|bulletSpeedInherit: 0.17619047619047616, |    bulletSpeedInherit: 0.47619047619047616,
|***wobject***
|distribution: 0.120517578125,       |    distribution: 0.030517578125,
|hitDamage: 2,          |    hitDamage: 0,
|***weapon***
|name: "FLAMER"         |    name: "FLAMER"
|ammo: 70,              |    ammo: 50,
|***wobject***
|timeToExplo: 30,       |    timeToExplo: 25,
|hitDamage: 2,          |    hitDamage: 1,
|***weapon***
|name: "CLUSTER POD"    |    name: "CLUSTER POD"
|parts: 19,             |    parts: 20,
|ammo: 2,               |    ammo: 1,
|delay: 40,             |    delay: 0,
|bulletSpeedInherit: 0.3142857142857143,      |    bulletSpeedInherit: 0.7142857142857143, 
|***wobject***
|hitDamage: 1,          |    hitDamage: 0,
|gravity: 0.01783642578125,  |    gravity: 0.01983642578125,
|timeToExplo: 12,       |    timeToExplo: 40,
|***weapon***
|name: "TUPOLEV"        |    name: "TUPOLEV"
|bulletSpeed: 1.7,      |    bulletSpeed: 2,
|bulletSpeedInherit: 0.2, |  bulletSpeedInherit: 0.5,
|***wobject***
|timeToExplo: 0,        |    timeToExplo: 105,
|partTrailDelay: 6,     |    partTrailDelay: 10,
|***nobject***
|gravity: 0.040517578125,    |    gravity: 0.030517578125,
|***weapon***
|name: "SCATTERGUN"     |    name: "SCATTERGUN"
|bulletSpeed: 2.9,      |    bulletSpeed: 2.5,
|***wobject***
|hitDamage: 6,          |    hitDamage: 3,
|***weapon***
|name: "MINIGUN"        |    name: "MINIGUN"
|***wobject***
|hitDamage: 4,          |    hitDamage: 2,
|***weapon***
|name: "DARTGUN"        |    name: "DARTGUN"
|loadingTime: 340,      |    loadingTime: 217,
|ammo: 8,               |    ammo: 6,
|delay: 9,              |    delay: 18,
|***wobject***
|hitDamage: 11,         |    hitDamage: 9,
|timetoExplo: 400,      |    timeToExplo: 300,
|blowAway: 0.5,         |    blowAway: 0.3,
|distribution: 0.03288818359375,         |    distribution: 0.02288818359375,
|***weapon***
|name: "LASER"          |    name: "LASER"
|***wobject***
|hitDamage: 1.3,        |    hitDamage: 1,
|***weapon***
|"MINI NUKE"            |    name: "MINI NUKE"
|***wobject***
|splinterAmount: 12,    |    splinterAmount: 8,
|***nobject***
|detectDistance: 2,     |    detectDistance: 1,
|speed: 0,              |    speed: 2,
|speedV: 0,             |    speedV: 1.2,
|distribution: 0.425,   |    distribution: 0.25,
|timeToExplo: 200,      |    timeToExplo: 80,
|timeToExploV: 30,      |    timeToExploV: 20,
|***weapon***
|name: "SPIKEBALLS"     |    name: "SPIKEBALLS",
|parts: 9,              |    parts: 7,
|bulletSpeed: 1.3,      |    bulletSpeed: 1.1,
|bulletSpeedInherit: 0.7090909090909091,  |   bulletSpeedInherit: 0.9090909090909091,
|***wobject***
|hitDamage: 12,         |    hitDamage: 9,
|timeToExplo: 260,      |    timeToExplo: 230,
|***weapon***
|name: "ZIMM"           |    name: "ZIMM"
|bulletSpeed: 3.1,      |    bulletSpeed: 3,
|bulletSpeedInherit: 0.1, |  bulletSpeedInherit: 0.3333333333333333,
|delay: 60,             |    delay: 90,
|***wobject***
|hitDamage: 50,         |    hitDamage: 49,
|timeToExplo: 380,      |    timeToExplo: 140,
|***weapon***
|name: "THROW KNIFE"    |    name: "THROW KNIFE"
|playReloadSound: false,|    playReloadSound: true,
|***wobject***
|hitDamage: 42,         |    hitDamage: 24,
|***weapon***
|name: "GAUSS GUN"      |    name: "GAUSS GUN"
|***wobject***
|detectDistance: 2,     |    detectDistance: 1,
|***weapon***
|name: "FLAK CANNON"    |    name: "FLAK CANNON"
|bulletSpeed: 5.3,      |    bulletSpeed: 5.1,
|***wobject***
|hitDamage: 18,         |    hitDamage: 15,
|***weapon***
|name: "PROXY MINE"     |    name: "PROXY MINE"
|***wobject***
|blowAway: 0.15,        |    blowAway: 0.22,
|***weapon***
|name: "SOLAR SCORCH"   |    name: "SOLAR SCORCH"
|***wobject***
|blowAway: 0.055,       |    blowaway: 0.015,
|***weapon***
|name: "CHIQUITA GUN"   |    name: "CHIQUITA GUN"
|parts: 5,              |    parts: 4,
|***weapon***
|added CRACKFIELD and CHIQUITA BOMB  |   - 
