# Zimmball Mod

by Kami

sprites are the same as Promode ReRevisited

# CHANGELOG

## 0.4

* rebalancing weapons (lower dmg)

## 0.31

* changes in physics (constans part)
* normal weapons: ZIMM, TACTICAL ZIMM, STATIC ZIMM
* defense-type weapons: HARD DODGE, SOFT DODGE
* NO WEAPON (useless, intended to fix an issue with random weapons when only 5 weapons are in the mod)
