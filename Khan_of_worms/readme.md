# Khan of Worms by Ed

Khan of Worms is a fast-paced mod based on steppe warrior mythology with new cool weapons. It has got also pretty cool custom sprites and palette. Optimized for Capture the Flag ([WebLiero Extended](https://www.vgm-quiz.com/dev/webliero/extended) game mode).

The mod was created using [WLedit](https://www.vgm-quiz.com/dev/webliero/wledit/), where you can also find its latest version.

The mod uses also custom soundpack which will work only for [WL Extended users](https://www.vgm-quiz.com/dev/webliero/extended-install). For non-extended players, the mod will still work but without any sounds.

current version: 1.20d
