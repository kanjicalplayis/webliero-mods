# Worms Armageddon by Daro

A Webliero mod with lots of new epic weapons, custom palette and sprites. Daro's opus magnum :P The main inspiration to make this mod was (as its name suggests) Worms Armageddon, however there are many other cool stuff here (especially lots of crazy weapons).

The main assumption of this mod was to make worms cannot dig the ground by themselves (as it was in Worms Armageddon), so be sure you equip drill or bore drill at the beginning of the match (those weapons are intended to dig dirt). However, if you want to activate worm digging ability back, you can do one of those 2 things:

1. restore default sprite 99;
2. set "0" or "1" for mFrame in dirt mask 7 (section dirteffect).

The weapons with names starting with "1" or "2" are very powerful and hence they're intended to be bonus-only, so to gain such effect in WL you need to ban them in room settings & set weapons for bonuses.

Enjoy!
