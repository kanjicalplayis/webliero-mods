# Webliero Mod Guide ⚙️

created by [jerrac](https://github.com/Scharnvirk), expanded by [roo](https://github.com/KangaRoo1372) & [vitu](https://github.com/Victorcorcos)

This mod guide will help you understand how to modify WL mods and even make your own ones. Enjoy!

Note: this file documents the purpose behind each parameter of the json/json5 file (which contains "logic" part of the mod). For making new and/or editing exitsing wlsprt file (which contains gfx changes of the mod), [see this guide made by wgetch](https://liero.phazon.xyz/wlsprt.html).

To create & modify mods for WebLiero, you can use [WLedit](https://www.vgm-quiz.com/dev/webliero/wledit/), which is a special tool designed for it, with lots of cool options and stuff. I strongly recommend it!

For more useful information about WebLiero and Liero game logic, you can visit [wgetch's liero pages](https://liero.phazon.xyz/) and [Liero Hacking Project site](https://liero.nl/lierohack/).

## Few general assumptions:
- Game does 60 physics ticks, or "frames", per second
- All times, durations etc. are shown in frames
- maximum amount of weapons in one mod is 255 (probably)
- every sObject MUST have startFrame (it cannot be -1)
- DON'T set splinterType -1 and splinterAmount > 0 at once (this will freeze the game)
- StartFrame: 0 doesn't work for nObjects (it works the same as StartFrame: -1)
- don't make weapons which generate big amount of powerful sObjects (with high range), it makes game laggy
- worm always bleeds on collision with sObject with damage > 0 and detectRange > 0 (unlike wObject and nObject which have bloodOnHit parameter)
- blowAway parameter in sObject doesn't work if its damage equals 0
- amount of blood created on worm collision with sObject is not calculated by damage, but detectRange parameter
- worm will not receive damage from nObject with hitDamage > 0 if you set TimeToExplo 1 (to make it work, TimeToExplo must be at least 2... or 0)
- there are some hardcoded things which you can't change in json file, i.e.:
1. time after bonuses explode: 75 seconds
2. sounds used for some events (death, hurt, alive, bump, reload, counter for last 10 seconds of match) cannot be changed (for different sound id)
3. you cannot disable rope completely (such feature was implemented in WL Extended room though)
4. worm starts bleeding when HP level is less than 25
5. mandatory objects: nObject6 is blood particle, nObject7 is shell, nObject0 is worm part (gibs)
6. when worm is killed, there will be generated always 8 worm parts (nObject0)
7. sObject0 is always used for explosion of bonuses
8. sObject7 is always used when bonuses appear on the map
9. nObject2 is created when there is powerful explosion (sObject) near dirt on the map
10. worm uses dirt mask 7 when digging dirt
11. wObject34: if you set "true" for "affectByExplosions" parameter, this object won't be pushed away by explosion's force (like other wObjects), but it will be removed from the map. That's because wObject34 was originally (in Liero 1.33) a booby trap and intended to behave like bonuses (bonuses explode on sObject explosion)
12. startFrame for flag object in flag-type game modes (you can modify the sprite itself, but cannot change the sprite id)
13. startFrame for health & weapon bonuses (you can modify the sprite itself, but cannot change the sprite id)
14. flag properties in flag-type game modes (gravity, bounce etc.)
15. sprites used for firecone (you can modify sprites, but cannot change sprites indices)
16. colours used for laserSight (you can change it by modifying palette, but you cannot change colour indices)
17. startFrame for hook in rope (you can modify the sprite itself, but cannot change the sprite id)
18. colours used for blood when it stains on rock/dirt/ground (you can change it by modifying palette, but you cannot change colour indices)
19. crosshair position in x/y offset
20. worm sprites IDs and their animation order (you can modify sprites, but cannot change sprite indices)

# Summary 🔍

1. [Soundpack](#soundpack-)
2. [Constants](#constants-)
3. [Weapons](#weapons-)
4. [wObjects](#wobjects-)
5. [nObjects](#nobjects-)
6. [sObjects](#sobjects-)
7. [Textures](#textures-)
8. [others](#others)
9. [Extended](#extended)

## Soundpack 🔉

Which `soundpack` is used.

Those are stored on the server side and cannot be changed or modified yet (unless you have [WebLiero extended hack installed](https://www.vgm-quiz.com/dev/webliero/extended)), you can only switch between few predetermined ones. Currently there are 6 predefined soundpacks:

1. sorliero.zip (dedicated for [Magic Playground](https://gitlab.com/webliero/webliero-mods/-/tree/master/MagicPlayground))
2. promode.zip (dedicated for [Promode Final](https://gitlab.com/webliero/webliero-mods/-/tree/master/pro_mode) & [Promode ReRevisited](https://gitlab.com/webliero/webliero-mods/-/tree/master/ReRevisited))
3. csliero.zip (dedicated for [csliero 2.2b](https://gitlab.com/webliero/webliero-mods/-/tree/master/csliero%202.2b))
4. blackwindv1.zip (dedicated for [Blackwind Liero TC](https://gitlab.com/webliero/webliero-mods/-/tree/master/%5Bold%20mods%20converted%5D/BLACKWIND%20-%20Blackwind%20TC%20v1))
5. blackwindv2.zip (dedicated for [Windstorm Liero TC](https://gitlab.com/webliero/webliero-mods/-/tree/master/%5Bold%20mods%20converted%5D/BLACKWIND%20-%20WINDSTORM%20Liero%20TC%20v2))
6. default soundpack for classic [Liero 1.33](https://gitlab.com/webliero/webliero-mods/-/tree/master/classic) (will be used if you don't use 'soundpack' function)

```js
soundpack: 'promode.zip',
```

## Constants 🕹️

The constants define general behavior and basic parameters of the mod, like physics, gravity, bonuses and ninja rope behaviors.

```js
    nrInitialLength: 250, // ninja rope initial length
    nrAttachLength: 28.125,    // ninja rope attach length
    minBounceUp: -0.8125,  // bounciness of worm when it hits sth while going up
    minBounceDown: 0.8125, // bounciness of worm when it hits sth while going down
    minBounceHoriz: 0.8125,  // bounciness of worm when it hits sth while going horizontally (left/right)
    wormGravity: 0.02288818359375,   // worm gravity
    wormAcc: 0.0762939453125,  // worm accelerate
    wormMaxVel: 0.969482421875,  // worm maximum velocity
    jumpForce: 0.85546875,   // worm jump force
    maxAimVel: 0.052430983960935, // max aim velocity
    aimAcc: 0.0029960562263391427, // max aim acceleration
    ninjaropeGravity: 0.0152587890625,   // ninja rope gravity
    nrMinLength: 10.625, // ninja rope minimum lenght; in liero its length could be adjusted
    nrMaxLength: 250,  // ninja rope maximum length
    bonusGravity: 0.02288818359375, // bonuses are health and ammo/weapon packs
    wormFricMult: 0.89, // worm friction multiplier
    aimFric: 0.83, // aim friction
    nrThrowVel: 8, // ninja rope throw velocity
    nrForce: 0.08333333333333333, // ninja rope pull strength
    bonusBounce: 0.4,  // bonus bounce parameter
    bonusFlickerTime: 220, // amount of time in game ticks before the bonus pack explodes
    aimMaxAngle: 1.5707963267948966,  // for using up arrow; 0 is looking right, 1/2π is aiming up
    aimMinAngle: -1.521708941582556,  // same as above but for using down arrow
    nrAdjustVel: 1.5, // ninja rope length adjust speed, probably unused
    nrColourBegin: 24,  // ninja rope colour begin; id taken from palette
    nrColourEnd: 27, // ninja rope colour end; id taken from palette
    bonusExplodeRisk: 100, // chances for a bonus to spontaneously explode
    bonusHealthVar: 20, // variance of health given by bonus
    bonusMinHealth: 10, // minimum bonus health. So in this setting healing range is 10-30
    firstBloodColour: 80,  // colour of blood as a trail; id taken from palette
    numBloodColours: 2,  // amount of colours to use for blood as a trail
    bObjGravity: 0.0152587890625, // gravity of blood as a trail
    splinterLarpaVelDiv: 3,  // diversification of velocity of nobjects when partTrailType: 1,
    splinterCracklerVelDiv: 3,  // diversification of velocity of nobjects when partTrailType: 0, 
    fallDamageHoriz: 0,  // dmg taken by worm when it hits sth while going horizontally (left/right) with certain speed
    fallDamageDown: 0,  // dmg taken by worm when it hits sth while going down with certain speed
    fallDamageUp: 0,  // dmg taken by worm when it hits sth while going up with certain speed
    hFallDamage: false, // hasFallDamage or not
    hBonusReloadOnly: true, // if true, weapon bonuses will only do reloads, if false - will give you a new weapon. Currently doesn't work in WL properly (even if set "true", it gives new weapon instead of doing reload only)
  },
```

## Weapons 🔫

A Weapon is a description on how the weapon itself works.

Think of it as if it was describing properties of the pistol itself, how does it affect you (recoil, noise, etc) but not the bullet.

```js
  weapons: [
    {
      /*
        How many objects does this weapon shoot. 1 for pistols, bazookas etc, 20 will be a shotgun-type weapon.
      */
      parts: 20,

      /*
        Which bullet type to use. Bullets are stored in an ordered array (counting is started from 0), so the only thing which matters is where it is in the array. I used comments denoting id of them - using a designation of weapon-id-x or "wid2" for example, but those are just that: comments. Only the actual position in array matters.
      */
      bulletType: 0,

      /*
         Base speed of the bullet. If too high, will pass through worms. 6 is about the max playable value for usual weapons.
         To make very fast gauss-like weapons, use "repeat" property.
      */
      bulletSpeed: 5,

      /*
        Percentage of worm's speed applied to the bullet. WARNING: This is affected by "repeat"!
      */
      bulletSpeedInherit: 0.2298850574712644,

      /*
        TODO: check, but I am pretty sure it doesn't do anything
      */
      distribution: 0.25,

      /*
        Recoil of the weapon
      */
      recoil: 0.55,

      /*
        Ammo per one magazine
      */
      ammo: 8,

      /*
        Delay between individual shots when shooting one magazine
      */
      delay: 28,

      /*
        Reload time
      */
      loadingTime: 400,

      /*
        Frequency of shells ejected. Set to 0,1,2,3 or 4. 0=Never, 1=Always, ..., 4=Rarely.
      */
      leaveShells: 1,

      /*
        Time between dropping shells. So for example you can make a minigun which drops shell every third shot to reduce map clutter.
      */
      leaveShellDelay: 28,

      /*
        Duration of firecone sprite being displayed. It is taken from the sprite sheet, is not animated and always uses same sprites (9-15, depending on crosshair position).
      */
      fireCone: 9,

      /*
        Whether a weapon has the flickering, red laser sight. It cannot be configured in any way, except for changing its colour (this requires palette changing tho).
      */
      laserSight: false,

      /*
        Whether a weapon has a laser beam of a given color configured within its weapon object (wobject) or not.
       
      */
      laserBeam: false,

      /*
        Sound to use when shooting. The only way to go check which is which is to go through all of them, since they are indexed by numbers. There are 30 sounds by default, counting from 0 to 29.
      */
      launchSound: 0,

      /*
        Play reload sound when weapon is reloaded or not.
      */
      playReloadSound: true,

      /* 
        Name of the weapon displayed when changing weapons and on bonus packs. Only capital letters work, so "Mn16" will show as "M".
      */
      name: 'AUTO SHOTGUN' 
    }

    // This repeats for all the other weapons defined in the game.
  ], // End of weapon descriptions' array
```

## wObjects 🚀

`wObjects` are **objects shot by weapons**. (like gunshots)

Only weapons can produce them, and ordering in the array matters, counting from 0.

So for example if you have a chaingun bullet stored in third position, you need to use "bulletType: 2" in your weapon - because it is counted from 0.

```js
  wObjects: [
    {
      // shotgun wid0 - Those are my comments denoting the id and the type of the object. I can quickly find an object I am looking for by seaching for "nids", "wids" and "sids".

      /*
        Additional worm detect distance for the bullet. Add more for "bigger" bullets or things like proximity detonators.
      */
      detectDistance: 1,

      /*
        Force affecting the hit worm. This will also work if the object has "wormCollide" set to false; it will simply not disappear and push the worm continuously.
      */
      blowAway: 0.01,

      /*
        Gravity of the projectile.
      */
      gravity: 0.01068115234375,

      /*
        Sound made on explosion. Note: an object may be removed without explosion, in which case no sound will be played - see "wormExplode" and "explGround".
      */
      exploSound: -1,

      /*
        Works in two modes.
        For shot types other than 2, this is additional speed added each frame. Use it for constant accelerating weapons.
        For shot type = 2 (directional player-controlled missile), this is an additional speed added to the missile when pressing up.
      */
      addSpeed: 0,

      /*
        Spread of the weapon. This works by adding a random direction vector of random length to current speed vector of the projectile.
      */
      distribution: 0.18310546875, 

      /*
        Speed multiplication each frame. Use it to have a weapons which accelerate or decelerate non-linearly, like proxy mine from promode.
      */
      multSpeed: 1,

      /*
        Which special object (sObject) to use on explosion. Those are stored in ordered arrays and are defined near the end of mod json file.
      */
      createOnExp: 2,

      /*
        Which dirt mask sprite to use (see "textures" part of the mod). Counting started from 0 (-1 is none).
      */
      dirtEffect: -1,

      /*
        Whether the object should explode (produce a sObject and a sound) on worm collision. Works only if "wormCollide" is set too.
      */
      wormExplode: false,

      /*
        Whether the object should explode on ground collision.
      */
      explGround: true,

      /*
        Whether the object should collide with the worm and get removed.
      */
      wormCollide: true,

      /*
        Whether the object should collide with other objects. If yes, they will bounce off themselves but none of them will be destroyed. If set to false, they pass through each other.
      */
      collideWithObjects: false,

      /*
        Whether the object is affected by explosions' push force. 
      */
      affectByExplosions: false,

      /*
        Speed multiply on bounce. Affects speed of bullet when bounces. After bouncing once the projectiles will get this percentage of their original speed. If you set it -1, then the bullet will pass through rock / dirt (note: works only for wObject, doesn't work for nObject).
      */
      bounce: 0,

       /*
       Bounciness of object. Affects the angle at which the projectile will bounce. 
      */
      bounceFriction: 0.8,

      /*
        Time to explode in frames. When set to 0 there will be no explosion at all. 
        Any positive value will cause creation of a designated special object (if not -1) and playing explosion sound.
      */
      timeToExplo: 0,

      /*
        Variation of time to explode in frames. TODO: Check how exactly is it factored in (edit: probably it defines maximum (negative) variation in time to explosion).
      */
      timeToExploV: 0,

      /*
        Damage inflicted on worm which was hit. Note: If the object does not have "wormCollide" property set, 
        this will be applied each frame the collision still occurs, leading to potentially huge damage values.
        If you set negative value for it, you will have healing effect.
      */
      hitDamage: 1,

      /*
        How much blood should be created on hit. This does not mean how many blood particles are created, treat it like a rough value.
      */
      bloodOnHit: 3,

      /*
        First sprite of animation used for this object.
        If -1, it will be a single pixel bullet using "colorBullets" for color.
      */
      startFrame: -1,

      /*
        Amount of sprites to use to animate the object, starting with "startFrame". 
        Note: Animation begins on random frame, so is suitable really only for objects which have animation cycle which looks good regardless
        of what frame it starts. Think things like spinning grenades, mines, pulsing items, etc. 
      */
      numFrames: 0,

      /*
        Whether the animation should be looped.
        TODO: Check if the animation is still random if loopAnim is set to false.
      */
      loopAnim: false,

      /*
        Defines general type of the weapon object. 
        0 - a standard object being either a colored pixel or animated sprite.
        1 - a missile-type object which uses one frame depending on its direction. "startFrame" defines start of directional sprite range in the spritesheet.
        2 - a player-controllable missile. It is animated like shotType: 1.
        3 - a missile-type object with "drunk" behavior when "distribution" is set to non-zero value. It is animated like shotType: 1.
        4 - in original Liero this was a "Laser-type" weapon (weapon still shoots bullets, but they will instantenously appear on target or wall. The "laser-type" weapon still obeys delay between shots, but drawing of the laser itself will not. Laser particles are drawn as long as you have some ammo and you press fire, even if the weapon is in its "delay" state). In webliero it most likely behaves just like shotType 0. TODO: verify if this indeed is the case.

        using shotType 4 is not recommended due to its uncertain behavior.
      */
      shotType: 0,

      /*
        How many times should the physics of this object be calculated per frame. Most notably, this affect percieved speed of the projectile.
        This is how you make very fast projectiles which do not pass through walls or worm.
        "shotType: 4" in original Liero was using repeat: 8 value, and there was no "repeat" property.
        So Gauss Gun, for example, should have "repeat": 8 and "shotType": 0 in webliero. Unlike original Liero, you can use any repeat value, but please note using absurdly high values may cause lags. 
      */
      repeat: 1,

      /*
        Color of the bullet. Works only if u use a pixel (startFrame: -1) for a wobject. Note: Some ranges in the palette are causing the bullet color to be slightly randomized.
        TODO: Verify how exactly does this work. 
      */
      colorBullets: 75,

      /*
        Amount of nObjects to create on explosion. The wObject must actually explode, for example if "wormExplode" is set to false and "wormCollide" is set to true, no nObjects will be created.
      */
      splinterAmount: 1,

      /*
        Color used on the nObjects, if they do not have animation set.
      */
      splinterColour: 13,
      
      /*
        Type of nObjects used when an explosion occurs. This refers to index of the nObject in the array (counting started from 0), so if you change order of the objects, something else will be used.
      */
      splinterType: 2,

      /*
        Way in which the splinter is scattered when the weapon explodes. 0=all directions (like big nuke), 1=direction the weapon is moving (like mini nuke).
      */
      splinterScatter: 0,

      /* 
        sObject (special Object) used as a trail. Special objects are basically explosions, they are static - cannot move or be affected in any way, but they can affect other objects and worms. Like every object type they are indexed by their order in the array (counting started from 0).
      */ 
      objTrailType: -1,

      /*
        Frame delay between creating trailing sObjects.
        Note: If "repeat" is set to greater than 1, sObjects will be created in "batches", creating intermittent lines instead of denser lines. 
        This probably is a bug or unintended behavior. 
      */
      objTrailDelay: 0,

      /*
        This is NOT a type of object used for trailing something.
        "partTrail" here refers to nObject trail, however partTrailType defines how will it be dropped:
        0 - crackler-type non-directional trail
        1 - larpa-type directional trail
        TODO: find out exactly how does this work
      */
      partTrailType: 0,

      /*
        This is the type of nObject trailed by the wObject.
      */
      partTrailObj: -1,

      /*
        Frame delay between creating trailed nObjects. 
        TODO: check if "repeat" greater than zero also affects this trail type like objTrailDelay.
      */
      partTrailDelay: 0,

      /*
        Base speed used for missile-type weapons ("shotType" = 2). Use "addSpeed" property to defined additional speed when pressing up while flying.
      */
      speed: 435
    }
    // (...) more WIDs follow here
    // (...) more WIDs follow here
    // (...) more WIDs follow here
    // (...) more WIDs follow here
  ],  // End of wObjects descriptions' array
```

## nObjects 🎈

`nObjects` are **non-weapon** objects.

Those are things like worm gibs, dropped shells, blood, but also additional weapon particles like Chiquita bananas dropped by Chiquita Bomb.

```js
  nObjects: [
    {
      // worm part nid0 - description of the nObject, this is a comment and as such does not interact with code at all.

      /*
        Additional worm detect distance for the bullet. Add more for "bigger" bullets or things like proximity detonators.
      */
      detectDistance: 0,

      /*
        Gravity of the particle.
      */
      gravity: 0.0152587890625,

      /*
        This is actual speed of the particle, used both for splinters and trails.
      */
      speed: 1.2,

      /*
        Speed variation. TODO: verify how exactly is it added/subtracted.
      */
      speedV: 0.4,

      /*
        Spread of the particle. This works by adding a random direction vector of random length to current speed vector of the projectile.
      */
      distribution: 0.1220703125,

      /*
        Force affecting the hit worm. This will also work if the object has "wormCollide" set to false; it will simply not disappear and push the worm continuously.
      */
      blowAway: 0,

      /*
        Bounciness of the object. Note: lack of "bounceFriction" property here!
      */
      bounce: 0.1,

      /*
        Damage inflicted on worm which was hit. Note: If the object does not have "wormDestroy" property set, 
        this will be applied each frame the collision still occurs, leading to potentially huge damage values.
        TODO: verify if "wormDestroy" is actually same sa "wormCollide" for wObjects.
      */
      hitDamage: 0,

      /*
        Whether the object should explode (produce a sObject and a sound) on worm collision. Works only if "wormCollide" is set too.
      */
      wormExplode: false,

      /*
        Whether the object should explode on ground collision.
      */
      explGround: true,

      /*
        Whether the object should collide with the worm and get removed.
      */
      wormDestroy: true,

      /*
        How much blood should be created on hit. This does not mean how many blood particles are created, treat it like a rough value.
      */
      bloodOnHit: 0,

      /*
        First sprite of animation used for this object.
        If -1 or 0, it will be a single pixel bullet using "colorBullets" for color.
      */
      startFrame: 165,

      /*
        Amount of sprites to use to animate the object, starting with "startFrame". 
        Note: Animation begins on random frame, so is suitable really only for objects which have animation cycle which looks good regardless of what frame it starts. Think things like spinning grenades, mines, pulsing items, etc. 
      */
      numFrames: 3,

      /* 
        When set to true, this makes the object to be drawn onto the map and object itself gets removed from the game.
        This is how mountains of shells and body parts are created in liero and also the reason why they clutter up the bunnyhoops.
      */
      drawOnMap: true,

      /*
        Color of the bullet. Works only if u use a pixel (startframe: -1 or 0) for a nobject. Note: Some ranges in the palette are causing the bullet color to be slightly randomized.
        TODO: Verify how exactly does this work. 
      */
      colorBullets: 0,

      /*
        Which special object (sObject) to use on explosion. Those are stored in ordered arrays and are defined near the end of mod json file (counting started from 0).
      */
      createOnExp: 4,

      /*
        Whether the object is affected by explosions' push force. 
      */
      affectByExplosions: false,

      /*
        Which dirt mask sprite to use (see "textures" part of json file). Counting started from 0 (-1 is none).
      */
      dirtEffect: -1,

      /*
        Amount of nObjects to create on explosion. The wObject must actually explode, for example if "wormExplode" is set to false and "wormCollide" is set to true, no nObjects will be created.
      */
      splinterAmount: 0,

      /*
        Color used on the nObjects, if they do not have animation set.
      */
      splinterColour: 0,

      /*
        Type of nObjects used when an explosion occurs. This refers to index of the nObject in the array (counting started from 0), so if you change order of the objects, something else will be used.
      */
      splinterType: -1,

      /*
        A nObject-specific property; when set to true, the nObject will trail blood particles.
      */
      bloodTrail: true,

      /*
        Delay between blood particles.
      */
      bloodTrailDelay: 10,

      /* 
        sObject (special Object) used as a trail. Special objects are basically explosions, they are static - cannot move or be affected in any way, but they can affect other objects and worms. Like every object type they are indexed by their order in the array (counting started from 0).
      */ 
      leaveObj: -1,

      /*
        Frame delay between creating trailing sObjects.
        Note: If "repeat" is set to greater than 1, sObjects will be created in "batches", creating intermittent lines instead of denser lines. 
        This probably is a bug or unintended behavior. 

        Note: notice that nObjects cannot trail other nObjects.
      */
      leaveObjDelay: 0,

      /*
        Time to explode in frames. When set to 0 there will be no explosion at all. 
        Any positive value will cause creation of a designated special object (if not -1) and playing explosion sound.
      */
      timeToExplo: 8000,

      /*
        Variation of time to explode in frames. TODO: Check how exactly is it factored in.
      */
      timeToExploV: 2000
    },
    // (...) all the other nObjects follow here
  ],  // End of nObjects descriptions' array
```

## sObjects 💥

`sObjects` are **special** Objects.

They are static objects which cannot be affected by anything. Their usual usage is to make explosions or non-movable trails.

```js
  sObjects: [
    {
      // large explosion sid0 - description of the sObject, this is a comment.

      /*
        sObjects create random sound on creation, use -1 for no sound.
        Start sound is first index used...
      */ 
      startSound: 9,

      /*
        ...and numSounds is amount of indices used to pick the starting sound from.
      */
      numSounds: 4,

      /*
        Delay before advancing to next frame.
      */
      animDelay: 2,

      /*
        First sprite of animation used for this object.
        Note: unlike for wObjects and nObjects, sObjects always start at proper startFrame.
        So if you set it -1, it will freeze the game.
      */
      startFrame: 40,

      /*
        Amount of sprites to use to animate the object, starting with "startFrame". 
      */
      numFrames: 15,

      /*
        Maximum detect range of the sObject when it begins to affect the worm.
      */
      detectRange: 20,

      /*
        Damage applied to the worm if in detect range. This is affected by how far the worm is from epicenter
        of the explosion. NOTE: it is very rare for an explosion to be in exact point where center sprite of the worm is.
        This means, usually the damage will be noticeably smaller than the number indicate. Use about 2/3 of its value
        as a rough estimate of the damage.
      */
      damage: 15,

      /*
        Force applied on the worm as push back. Can also be negative.
        TODO: Document the quirks of this. The whole x-y axis situation.
      */
      blowAway: 0.0457763671875,

      /*
        Whether the sObject should create a shadow. nObjects and wObjects always do.
        Currently does not work in WL.
      */
      shadow: true,

      /*
        Unused, in Liero explosions could shake and flash. This is not implemented yet in WL.
      */
      shake: 4,

      /*
        Unused, in Liero explosions could shake and flash. This is not implemented yet.
      */
      flash: 8,

      /*
        Which dirt mask sprite to use (see "textures" part). Counting started from 0 (-1 is none).
      */
      dirtEffect: 0
    },
    // rest of sObjects follow here
  ], // End of sObjects descriptions' array
```

## Textures 🎨

These `textures` parameters has the reference in `nObject`, `wObject` and `sObject` as `dirtEffect`s.

`dirtEffect` is a parameter present on all object types (`nObject`, `wObject` and `sObject`).

Changes here affects all dirts on the map and how the gunshots interact with them.

```js
  textures: [  // dirt masks
    {
      nDrawBack: true,  // Causes Liero not to draw the anti-alias edges on the background. Normally turned "false" for dirt and rock & turned "true" for cleaning dirt.
      mFrame: 0,  // Controls which sprite is used to cut a hole (= size and shape of the hole).
      sFrame: 73,  // the texture the map change will leave behind (= which sprite is used to fill the hole).
      rFrame: 2  // the amount of sprites to use to fill the hole (starting from sFrame).
    },
   
   // rest of textures follow here
  ], // End of textures descriptions' array
```

## others

other parameters which are usually defined in the last part of json file.

```js
  colorAnim: [129, 131, 133, 136, 152, 159, 168, 171],  // list of colours (ID taken from the palette) which will be animated.
  textSpritesStartIdx: 240,  // starting sprite for letters used for weapon names (which are displayed above the worm when you change weapons during game)
  crossHairSprite: 153,  // starting sprite for crosshair used by worm.
  name: "Promode ReRevisited", // name of the mod; will be displayed in the room options menu.
  author: "Scharnvirk-jerrac", // author of the mod
  version: "1.3.2", // version of the mod
  }  // End of the mod file 
```

## extended

there are some new cool parameters implemented to WebLiero via [WebLiero extended hack](https://www.vgm-quiz.com/dev/webliero/extended). They work only if you use WL Extended.

- **noRope** _(boolean, section "constans")_ - disables rope when set "true"
- **removeOnSObject** _(boolean, for wObject)_ - if set "true", object will be removed on collision with sObject (like wObject34)
- **immutable** _(boolean, for wObject and nObject)_ - prevents object to be moved by other wObjects with "collideWithObjects" property set "true"
- **fixed** _(boolean, for wObject)_ - prevents object to be affected by its own velocity (object doesn't move even it has speed > 0)
- **teamImmunity** _(int, for wObject and nObject)_ - defines which team (in team game modes) won't take damage from this object (0/1/2)
- **detonable** _(boolean, for wObject)_ - if wObject explodes remotely by using a detonator
- **platform** _(boolean, for wObject)_ - makes object behave like a platform that you can hook rope into
- **platformWidth** _(int, for wObject)_ - sets platform width (size in X offset)
- **platformHeight** _(int, for wObject)_ - sets platform height (size in Y offset)
- **platformVelocityAuto** _(boolean, for wObject)_ - whether platform transfers its velocity to a worm
- **reloadSound** _(int, section "weapons")_ - sets custom sound for reload, none (-1) for default
