# Wojenny Mod (War Mod) Revisited by Larcelo

A Webliero mod with realistic weapons inspired by war games. It has got also pretty cool custom sprites. The biggest feature of this mod is that almost all weapons can destroy the solid rock.

The mod was originally created as a Liero 1.36 TC, so that some things had to be changed to adapt it to Webliero, that is:

1) one weapon was removed (P ATOMOWY - because this weapon caused the game crash due to too many sobjects with high dmg);
2) rope was changed (nrMinLength, nrMaxLength, nrThrowVel);
3) weapon names were translated into English (originallly the names were in Polish).

(latest version of this mod can be found in [WLedit mod base](https://www.vgm-quiz.com/dev/webliero/wledit/)).

# CHANGELOG

## 15-11-2021

- new weapons added
- some changes in weapons
- soundpack taken from csliero
