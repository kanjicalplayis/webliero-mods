# Wojenny Mod (War Mod) by Larcelo

A Webliero mod with realistic weapons inspired by war games. It has got also pretty cool custom sprites. The biggest feature of this mod is that almost all weapons can destroy the solid rock.

This mod version is based on the [version uploaded by ChanibaL on Liga Liero Forum.](https://www.liero.org.pl/forum/index.php?action=tpmod;dl=item73) You can watch the original gameplay [here.](https://www.youtube.com/watch?v=aU1EC4oAg7Y)

However, Larcelo has already created further versions of his mod, one of which you can find in the subfolder "Wojenny revisited" (version 15-11-2021). Larcelo made many cool changes in this version, comparing to the first one (including some new weapons added).
