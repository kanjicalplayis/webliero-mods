# Wojenny Mod (War Mod) by Larcelo

Here you can find webliero-fixed version of Larcelo's Wojenny Mod (War Mod).

The mod was originally created as a Liero 1.36 TC, so that some things had to be changed to adapt it to Webliero, that is:

1) one weapon was removed (P ATOMOWY - because this weapon caused the game crash due to too many sobjects with high dmg);
2) rope was changed (nrMinLength, nrMaxLength, nrThrowVel);
3) weapon names were translated into English (originallly the names were in Polish).

This mod version is based on the [version uploaded by ChanibaL on Liga Liero Forum.](https://www.liero.org.pl/forum/index.php?action=tpmod;dl=item73) You can watch the original gameplay [here.](https://www.youtube.com/watch?v=aU1EC4oAg7Y)

However, Larcelo has already created further versions of his mod, one of which you can find in the subfolder "Wojenny revisited" (version 15-11-2021). Larcelo made many cool changes in this version, comparing to the first one (including some new weapons added).
