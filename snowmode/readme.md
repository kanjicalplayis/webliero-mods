# snowmode by Snowman (with sprites fixed by Kangur)

Another nice WebLiero mod based on & inspired by jerrac's Promode ReRevisited, with 32 rebalanced weapons (including 7 new ones: AURA, MINI MISSILES, NAPALM MISSILE, PHOENIX, PIXEL BOMB, SONIC WAVE and UZI) and custom sprites.

For more information about this mod and full changelog, visit [Snowman's github](https://github.com/xsnowman/liero-snowmode).