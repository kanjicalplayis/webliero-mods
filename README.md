# Webliero Mods Repository

## Webliero
Webliero is an online version of excellent game Liero made by Joosa Riekkinen and released in 1998.
You can play it for free in your browser at https://www.webliero.com/

## How to use the mods
This repo contains user-made mods for the game. 
To use a mod:
- download all its files (`.json5` and `.wlsprt` files) from relevant folder
- open chat (`enter` by default)
- type `/loadmod` and select both `mod.json5` (this contains "logic" changes of the mod) and `sprites.wlsprt` (this contains gfx changes of the mod)

Note, the `.json5` and `.wlsprt` files can be named any way you want, but usually they will be like above.

WebLiero supports also .zip files for bundling the wlsprt and json / json5 files together when using /loadmod command, but the zip needs to contain both mod files in the root (meaning no folders inside) and the files must be named `mod.json5` and `sprites.wlsprt`. 
  
You can find direct links to the files here: https://webliero.gitlab.io/webliero-mods/

including zipped versions of the mods and pool (zips.json)
