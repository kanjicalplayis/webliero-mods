# BUILDING GAME

this mod is a building game

you can create a map in-game with it

## v0.7.1

* SPRAY: FOR RYSIEK with no delay
* PAINT BRUSH: missile type weapon drawing over every type of material
## v0.6.6

* FOR RYSIEK: dot drawing weapon for parkur
* LINE MARKER: drawing straight lines

## v0.5.0

* PENCIL: drawing missile type weapon

useful to clear out dirt "holes"

## v0.4.1

* FLAMING ROCKER THAT SUCKS: flaming type weapon to print rock (color idx 19), mostly works vertically (from a pixel bellow or above), kinda useful to make walls and towers, or fill evenly with rock 

## v0.4.0

* DIRT FILLER: new missile type weapon, draws rock (color idx 19) over dirt and only dirt!

useful to clear out dirt "holes"

## v0.3.0

* new building bricks:
  TETRIS ANGLE BLUE
  TETRIS ANGLE GREEN
  TETRIS ANGLE GREY
  TETRIS ANGLE RED
  TETRIS SQUARE GREY
  TETRIS TEEWEE BLUE
  TETRIS TEEWEE GREEN


## v0.2.1

no new weapons, but huge improvement on usability of bricks thanks to Szarnywirk's suggestions @ https://discord.com/channels/435544640599490560/515507463735214091/782706688725745724

bricks don't require a rock pixels to be placed since a small dirt effect is used to capture them in place

## v0.1.1

* GOO ROCK: new "goo like rock utility weapon", can be used to "fill" some holes, add rock pixels on the ground when it got completely cleared, or make drops of rock anywhere

## v0.0.1

first version, adds a few brick with different colors, a square & a boulder, and a few more "tetrises" including some to write "LIERO" in tetris bricks

bricks can be placed on "at least" a pixel of rock, else they won't be printed on the map

* building bricks:
  SQUARE
  BLUE BRICK
  GREEN BRICK
  GREY BRICK
  RED BRICK
  BOULDER
  TETRIS LEFT
  TETRIS RIGHT
  TETRIS VERTICAL
  TETRIS HORIZONTAL
  TETRIS E
  TETRIS R
  TETRIS O
  TETRIS BIG L

* utilities:
  RUBBER ERASER: used to clear small bits of rock or dirt
  FLAMING ERASER: used to clear wider parts of rock or dirt
  SUICIDE: useful whenever you get stuck
