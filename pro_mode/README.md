# Liero Pro Mode Final mod by Biernath John

This is the original, unchanged variant of Liero Promode Final, created by Biernath John. One of the default mods in Webliero.

## How to use

To use the mod:
- download all its files from this folder
- open chat (`enter` by default)
- type `/loadmod` and select both `mod.json5` (this contains "logic" changes of the mod) and `sprites.wlsprt` (this contains gfx changes of the mod)
